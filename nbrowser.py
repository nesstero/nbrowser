#! /usr/bin/env python

from sys import argv
import argparse
from PyQt5.QtCore import QUrl
from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5.QtWebEngineWidgets import QWebEngineView

class windowNb(QMainWindow):
    
    def __init__(self):
        super().__init__()
        self.arg = argparse.ArgumentParser()
        self.arg.add_argument("-u", "--url", required=True, help="Alamat Url contoh youtube.com")
        self.args = vars(self.arg.parse_args())
        self.nburl = f"https://{self.args['url']}"
        self.headersUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 11.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.144 Safari/537.36"
        self.nbrowser = QWebEngineView()
        self.nbrowser.page().profile().setHttpUserAgent(self.headersUserAgent)
        self.nbrowser.load(QUrl(self.nburl))
        self.setCentralWidget(self.nbrowser)
        self.showMaximized()

app = QApplication(argv)
QApplication.setApplicationDisplayName("N-Browser")
window = windowNb()
app.exec()
